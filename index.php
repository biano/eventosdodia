<?php 
	//Carrega o cabeçalho do site
get_header();

?>


<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
	<!-- Indicators -->
	<ol class="carousel-indicators">
		<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
		<li data-target="#carousel-example-generic" data-slide-to="1"></li>
		<li data-target="#carousel-example-generic" data-slide-to="2"></li>
	</ol>

	<!-- Wrapper for slides -->
	<div class="carousel-inner" role="listbox">
		<div class="item active">
			<img src="<?php bloginfo('template_directory'); ?>/assets/images/slide-01.jpg" alt="Atendimento personalizado">
			<div class="carousel-caption">
				<h1>Imagem 01</h1>
			</div>
		</div>
		<div class="item">
			<img src="<?php bloginfo('template_directory'); ?>/assets/images/slide-02.jpg" alt="Agendamento de consulta on-line">
			<div class="carousel-caption">
				<h1>Imagem 02</h1>
			</div>
		</div>
			<!-- <div class="item">
				<img src="images/slide-03.jpg" alt="...">
				<div class="carousel-caption">
					<h1>Imagem 03</h1>
				</div> -->
			</div>
		</div>

		<!-- Controls -->
		<a class="left" href="#carousel-example-generic" role="button" data-slide="prev">
			<!-- <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
			<span class="sr-only">Previous</span> -->
		</a>
		<a class="right" href="#carousel-example-generic" role="button" data-slide="next">
			<!-- <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
			<span class="sr-only">Next</span> -->
		</a>
	</div>
	
	<section class="conteudo">
		<div class="container">
			<h3>empresa</h3>
			<div class="row">
				<?php 
				$args = array('post_type' => 'evento');
				$loop = new WP_Query($args);
				if ($loop->have_posts()) : ?>
				<?php while ($loop->have_posts()) : $loop->the_post(); ?>    
					<div class="col-md-4 col-sm-4 col-xs-4">
						<figure>
							<a href="<?php the_permalink(); ?>">
								<?php the_post_thumbnail( 'thumb-index' ); ?> 
								<figcaption><h2><span><?php echo get_post_meta( get_the_ID(), 'estado_evento', true ); ?></span>
								<span><?php echo get_post_meta( get_the_ID(),'titulo_evento', true); ?></span></h2></figcaption>
							</a>
						</figure>
					</div>
				<?php endwhile; ?>
			<?php endif; ?>

				<!--
				<div class="col-md-4 col-sm-4 col-xs-4"">
					<figure>
						<img src="<?php //bloginfo('template_directory'); ?>/assets/images/mimagens.jpg" alt="icone hospital" class="center-block" />
						<figcaption><h2><span>SP</span><span>nome da cidade</span></h2></figcaption>
					</figure>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-4"">
					<figure>
						<img src="<?php //bloginfo('template_directory'); ?>/assets/images/mimagens.jpg" alt="icone hospital" class="center-block" />
						<figcaption><h2><span>SP</span><span>nome da cidade</span></h2></figcaption>
					</figure>
				</div>
			-->

		</div>
	</div>
</section>


<?php
	//Carrega o rodape do site 
get_footer();
?>