<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>
		<?php if (is_home()){
			bloginfo('name');
		}elseif (is_category()){
			single_cat_title(); echo ' -  ' ; bloginfo('name');
		}elseif (is_single()){
			single_post_title();
		}elseif (is_page()){
			bloginfo('name'); echo ': '; single_post_title();
		}else {
			wp_title('',true);
		} ?>
	</title>

	<!-- Local bootstrap CSS & JS & Font Weasome-->
	<link rel="stylesheet" media="screen" href="<?php bloginfo('template_directory'); ?>/assets/css/bootstrap.min.css">
	<link rel="stylesheet" media="screen" href="<?php bloginfo('template_directory'); ?>/assets/css/style.css">

	<link rel="stylesheet" media="screen" href="<?php bloginfo('template_directory'); ?>/assets/css/font-awesome.min.css">
	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Archivo+Black&amp;subset=latin-ext" rel="stylesheet">
	
	<script src="<?php bloginfo('template_directory'); ?>/assets/js/jquery-3.2.1.min.js"></script>
	<script src="<?php bloginfo('template_directory'); ?>/assets/js/bootstrap.min.js"></script>
	
</head>
<body onload="header()">
	<header id="diminuir">
		<nav id="menu-Principal" class="navbar navbar-default menu-Principal">
			<div class="container">
 					<div class="col-md-4">
						<!-- Brand and toggle get grouped for better mobile display -->
						<div class="navbar-header">
							<h1 class="logo-principal"><a id="teste" class="navbar-brand" href="
								<?php echo get_settings('home'); ?>">Brand</a></h1>
						</div>
					</div>
					<div id="nav-principal" class="col-md-5">
						<!-- Collect the nav links, forms, and other content for toggling -->
						<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
							<ul class="nav navbar-nav">
								<li class="active"><a href="<?php echo get_settings('home'); ?>">Link <span class="sr-only">(current)</span></a></li>
								<li><a href="#">Link</a></li>
								<li class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
									<ul class="dropdown-menu">
										<li><a href="#">Action</a></li>
										<li><a href="#">Another action</a></li>
										<li><a href="#">Something else here</a></li>
										<li role="separator" class="divider"></li>
										<li><a href="#">Separated link</a></li>
										<li role="separator" class="divider"></li>
										<li><a href="#">One more separated link</a></li>
									</ul>
								</li>
							</ul>
						</div><!-- /.navbar-collapse -->
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
					</div>
					<div id="rsociais" class="col-md-3">
						<ul class="nav navbar-nav navbar-right">
							<li><a href="#"><i class="fa fa-facebook-square fa-3x" aria-hidden="true"></i></a></li>
							<li><a href="#"><i class="fa fa-twitter-square fa-3x" aria-hidden="true"></i></a></li>
							<li><a href="#"><i class="fa fa-youtube-square fa-3x" aria-hidden="true"></i></a></li>
						</ul>
					</div>

				</div>


			</div><!-- /.container-fluid -->
		</nav>
	</header>
