<?php include "header.php"; ?>

	<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
		<!-- Indicators -->
		<ol class="carousel-indicators">
			<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
			<li data-target="#carousel-example-generic" data-slide-to="1"></li>
			<li data-target="#carousel-example-generic" data-slide-to="2"></li>
		</ol>

		<!-- Wrapper for slides -->
		<div class="carousel-inner" role="listbox">
			<div class="item active">
				<img src="images/slide-01.jpg" alt="Atendimento personalizado">
				<div class="carousel-caption">
					<h1>Imagem 01</h1>
				</div>
			</div>
			<div class="item">
				<img src="images/slide-02.jpg" alt="Agendamento de consulta on-line">
				<div class="carousel-caption">
					<h1>Imagem 02</h1>
				</div>
			</div>
			<!-- <div class="item">
				<img src="images/slide-03.jpg" alt="...">
				<div class="carousel-caption">
					<h1>Imagem 03</h1>
				</div> -->
			</div>
		</div>

		<!-- Controls -->
		<a class="left" href="#carousel-example-generic" role="button" data-slide="prev">
			<!-- <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
			<span class="sr-only">Previous</span> -->
		</a>
		<a class="right" href="#carousel-example-generic" role="button" data-slide="next">
			<!-- <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
			<span class="sr-only">Next</span> -->
		</a>
	</div>
	
	<section class="conteudo">
		<div class="container">
			<h3>empresa</h3>
			<div class="row">
				<div class="col-md-4 col-sm-4 col-xs-4">
					<figure>
						<img src="images/mimagens.jpg" alt="icone hospital" class="center-block" />
						<figcaption><h2><span>SP</span><span>nome da cidade</span></h2></figcaption>
					</figure>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-4"">
					<figure>
						<img src="images/mimagens.jpg" alt="icone hospital" class="center-block" />
						<figcaption><h2><span>SP</span><span>nome da cidade</span></h2></figcaption>
					</figure>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-4"">
					<figure>
						<img src="images/mimagens.jpg" alt="icone hospital" class="center-block" />
						<figcaption><h2><span>SP</span><span>nome da cidade</span></h2></figcaption>
					</figure>
				</div>

			</div>
		</div>
	</section>
	<!--
	<section id="servicos">
		<div class="container">
			<h2>serviços</h2>
			<div class="row center-block">
				<div class="col-md-4">
					<div class="border-4">
						<div class="icones bloco-notas">
						</div>
						<div class="titulo-icones">
							<h3>ppra e pcmso</h3>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="border-4">
						<div class="icones cruz"></div>
						<h3>aso</h3>
					</div>
				</div>
				<div class="col-md-4">
					<div class="border-4">
						<div class="icones saudacao"></div>
						<h3>consultoria e treinamento</h3>
					</div>
				</div>
				<div class="col-md-4">
					<div class="border-4">
						<div class="icones exame-adicional"></div>
						<h3>exame admissional e demissional</h3>
					</div>
				</div>
				<div class="col-md-4">
					<div class="border-4">
						<div class="icones exame-laboratorio"></div>
						<h3>exames laboratoriais</h3>
					</div>
				</div>
				<div class="col-md-4">
					<div class="border-4">
						<div class="icones exame-raio-x"></div>
						<h3>exames raio x</h3>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section id="orcamento">
		<div class="container">
			<h2>orçamento</h2>
			<h3>LOREM IPSUM, DISPUTIM.</h3>

			<p>Proin felis nunc, auctor sed urna ac, congue sagittis quam. Suspendisse hendrerit, quam</p>

			<form>
				<div class="form-group">
					<label for="nome">Nome Completo: *</label>
					<input type="text" class="form-control" id="nome" placeholder="Nome Completo">
				</div>

				<div class="row">
					<div class="form-group col-md-6">
						<label>email</label>
						<input type="text" name="cidade" class="form-control" id="cidade" placeholder="Informe sua Cidade">
					</div>
					<div class="form-group col-md-6">
						<label>telefone</label>
						<input type="text" name="estado" class="form-control" id="estado" placeholder="Estado">
					</div>
				</div>
				<div class="row">
					<div class="form-group col-md-6">
						<label>empresa</label>
						<input type="text" name="ddd" class="form-control" id="ddd" placeholder="DDD">
					</div>
					<div class="form-group col-md-6">
						<label>cnpj</label>
						<input type="text" name="telefone" class="form-control" id="telefone" placeholder="0000-0000">
					</div>
				</div>
				<div class="row">
					<div class="form-group col-md-6">
						<label>nº funcionarios: *</label>
						<input type="email" name="email" class="form-control" id="email" placeholder="Informe seu email">
					</div>
					<div class="form-group col-md-6">
						<label>serviço ou treinamento: *</label>
						<input type="email" name="email" class="form-control" id="email" placeholder="Informe seu email">
					</div>
				</div>

				<div class="form-group">
					<label>Mensagem:</label>
					<textarea class="form-control" rows="4"></textarea>

				</div>
				
				<div class="form-group">
					<button type="button" class="btn btn-atami">Enviar</button>
					<button type="button" class="btn btn-atami" disabled="disabled">limpar</button>
				</div>
			</form>
		</div>
	</section>

<section id="contato">
	<div class="container">
		<h2>contato</h2>
		<h3>LOREM IPSUM, DISPUTIM.</h3>

		<p>Proin felis nunc, auctor sed urna ac, congue sagittis quam. Suspendisse hendrerit, quam</p>

		<h3>LOCALIZAÇÃO</h3>
		<p>Rua Benedito Kuhl, nº 800, Vila Cláudia - Limeira - SP</p>
		<p>CEP 13480-410</p>
		<p>Fone (019) 3442-3647</p>

		<form>
			<div class="form-group">
				<label for="nome">Nome Completo: *</label>
				<input type="text" class="form-control" id="nome" placeholder="Nome Completo">
			</div>

			<div class="row">
				<div class="form-group col-md-6">
					<label>Cidade</label>
					<input type="text" name="cidade" class="form-control" id="cidade" placeholder="Informe sua Cidade">
				</div>
				<div class="form-group col-md-6">
					<label>Estado</label>
					<input type="text" name="estado" class="form-control" id="estado" placeholder="Estado">
				</div>
			</div>
			<div class="form-group">
				<label>Mensagem:</label>
				<textarea class="form-control" rows="4"></textarea>

			</div>
			<div class="form-group">
				
				<button type="button" class="btn btn-atami">Enviar</button>
				<button type="button" class="btn btn-atami" disabled="disabled">limpar</button>
			</div>
		</form>
	</div>
</section>
<section id="como-chegar">
	<div class="container">
		<h2>como chegar</h2>
		<!-- coordenada - [google maps] -22.577266, -47.399633 -->
		<!--<div id="map"></div>-->
		<!--script>
			/*function initMap() {
				var uluru = {lat: -22.576398, lng: -47.399594};
				var map = new google.maps.Map(document.getElementById('map'), {
					zoom: 15,
					center: uluru
				});
				var marker = new google.maps.Marker({
					position: uluru,
					map: map,
					animation: google.maps.Animation.BOUNCE,
					icon:'images/maps-google.png'
				});
				var infoWindow = new google.maps.InfoWindow({
					content: "Dimestra - Medicina Ocupacional"
				});
				infoWindow.open(map, marker);
			}
		</script-->
		<!--script async defer
		src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBsssZSuhYy92rUxrz08UTd-3GDM2sCNKI&callback=initMap">
	</script-->
<!--
</div>
</section-->
<?php include "footer.php"; ?>