<?php 
/*
Template Name: Contato
*/
	//Carrega o cabeçalho do site
	get_header();
?>

	<section class="conteudo">
		<div class="container">
			<h3>Detalhes do evento</h3>
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<figure>
						<img src="<?php bloginfo('template_directory'); ?>/assets/images/imagem-g-eventos.jpg" alt="icone hospital" class="center-block" />
						<figcaption><h2><span>SP</span><span>nome da cidade</span></h2></figcaption>
					</figure>
				</div>
				<div class="col-md-8 col-sm-8 col-xs-8"">
					<h3><?php the_title(); ?></h3>
					<p><?php echo get_post_meta( get_the_ID(), 'descricao_evento', true ); ?></p>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-4 teste">
					<h3><?php echo get_post_meta( get_the_ID(), 'titulo_evento', true ); ?></h3>
					<ul>
						<li>
							<button type="button" class="btn btn-facebook"><i class="fa fa-facebook fa-3x" aria-hidden="true"></i>facebook</button>
						</li>
						<li>
							<button type="button" class="btn btn-twitter"><i class="fa fa-twitter fa-3x" aria-hidden="true"></i>twiitter</button>
						</li>
						<li>
							<button type="button" class="btn btn-google"><i class="fa fa-google fa-3x" aria-hidden="true"></i>google</button>
						</li>

					</ul>
					
				</div>
			</div>
			<h3>Como chegar</h3>
			<div class="col-md-6 col-sm-6 col-xs-6">
				<h2>google maps</h2>
			</div>
			<div class="col-md-6 col-sm-6 col-xs-6">
				<h3>Pontos de referencia:</h3>
			</div>
		</div>
		
	</section>

<?php
	//Carrega o rodape do site 
	get_footer();
?>