<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Dimestra Ocupacional - Medicina Ocupacional</title>

	<!-- Local bootstrap CSS & JS & Font Weasome-->
	<link rel="stylesheet" media="screen" href="css/bootstrap.min.css">
	<link rel="stylesheet" media="screen" href="css/style.css">

	<link rel="stylesheet" media="screen" href="css/font-awesome.min.css">
	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Archivo+Black&amp;subset=latin-ext" rel="stylesheet">
	
	<script src="js/jquery-3.2.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	
</head>
<body onload="header()">
	<header id="diminuir">
		<nav id="menu-Principal" class="navbar navbar-default menu-Principal">
			<div class="container">
				<div class="row">
					<div class="col-md-4">
						<!-- Brand and toggle get grouped for better mobile display -->
						<div class="navbar-header">
							<h1 class="logo-principal"><a id="teste" class="navbar-brand" href="#">Brand</a></h1>
						</div>
					</div>
					<div id="nav-principal" class="col-md-4">
						<!-- Collect the nav links, forms, and other content for toggling -->
						<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
							<ul class="nav navbar-nav">
								<li class="active"><a href="#">Link <span class="sr-only">(current)</span></a></li>
								<li><a href="#">Link</a></li>
								<li class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
									<ul class="dropdown-menu">
										<li><a href="#">Action</a></li>
										<li><a href="#">Another action</a></li>
										<li><a href="#">Something else here</a></li>
										<li role="separator" class="divider"></li>
										<li><a href="#">Separated link</a></li>
										<li role="separator" class="divider"></li>
										<li><a href="#">One more separated link</a></li>
									</ul>	
								</li>
							</ul>
						</div><!-- /.navbar-collapse -->
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
					</div>
					<div id="rsociais" class="col-md-4">
						<ul class="nav navbar-nav navbar-right">
							<li><a href="#"><i class="fa fa-facebook-square fa-3x" aria-hidden="true"></i></a></li>
							<li><a href="#"><i class="fa fa-twitter-square fa-3x" aria-hidden="true"></i></a></li>
							<li><a href="#"><i class="fa fa-youtube-square fa-3x" aria-hidden="true"></i></a></li>
						</ul>
					</div>

				</div>


			</div><!-- /.container-fluid -->
		</nav>
	</header>
	
	<section class="conteudo">
		<div class="container">
			<h3 style="width: 266px">sobre a empresa</h3>
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<p>Lorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos. Lorem Ipsum sobreviveu não só a cinco séculos, como também ao hammersalto para a editoração eletrônica, permanecendo essencialmente inalterado. Se popularizou na década de 60, quando a Letraset lançou decalques contendo passagens de Lorem Ipsum, e mais recentemente quando passou a ser integrado a softwares de editoração eletrônica como Aldus PageMaker.</p>

					<p>Lorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos. Lorem Ipsum sobreviveu não só a cinco séculos, como também ao hammersalto para a editoração eletrônica, permanecendo essencialmente inalterado. Se popularizou na década de 60, quando a Letraset lançou decalques contendo passagens de Lorem Ipsum, e mais recentemente quando passou a ser integrado a softwares de editoração eletrônica como Aldus PageMaker.</p>

					<div class="separator"></div>
				</div>
				<div class="col-md-6 col-sm-6 col-xs-6"">
					<img src="images/imagem-empresa.jpg" alt="icone hospital" class="center-block" />
				</div>
				<div class="col-md-6 col-sm-6 col-xs-6 teste">
					<img src="images/imagem-empresa.jpg" alt="icone hospital" class="center-block" />
				</div>
			</div>
			<h3 style="width: 250px">Como chegar</h3>
			<div class="col-md-6 col-sm-6 col-xs-6">
				<p>Lorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos. Lorem Ipsum sobreviveu não só a cinco séculos, como também ao hammersalto para a editoração eletrônica, permanecendo essencialmente inalterado. Se popularizou na década de 60, quando a Letraset lançou decalques contendo passagens de Lorem Ipsum, e mais recentemente quando passou a ser integrado a softwares de editoração eletrônica como Aldus PageMaker.</p>
				<div class="separator"></div>
			</div>
			<div class="col-md-6 col-sm-6 col-xs-6">
				<img src="images/imagem-empresa.jpg" alt="icone hospital" class="center-block" />

				<div class="separator"></div>
			</div>
			<div class="col-md-3 col-sm-3 col-xs-3">
				<img src="images/mimagens.jpg" alt="icone hospital" class="center-block" />		
			</div>
			<div class="col-md-3 col-sm-3 col-xs-3">
				<img src="images/mimagens.jpg" alt="icone hospital" class="center-block" />		
			</div>
			<div class="col-md-3 col-sm-3 col-xs-3">
				<img src="images/mimagens.jpg" alt="icone hospital" class="center-block" />		
			</div>
			<div class="col-md-3 col-sm-3 col-xs-3">
				<img src="images/mimagens.jpg" alt="icone hospital" class="center-block" />		
			</div>

		</div>
	</section>

	<footer>
		<div class="rodape">
			<div class="container">
				<div class="row">
					<div class="col-md-4 col-sm-4 col-xs-4">
						<div class="endereco">
							<h4>acesso facil:</h4>
							<ul>
								<li><a href="#">home</a></li>
								<li><a href="#">sobre empresa</a></li>
								<li><a href="#">cadastrar evento</a></li>
								<li><a href="#">principais eventos</a></li>
								<li><a href="#">contato</a></li>
							</ul>
						</div>
					</div>
					<div class="col-md-4 col-sm-4 col-xs-4">
						<div class="endereco">
							<h4>nossa localização:</h4>
							<p><span>endereço:</span>Rua Benedito Kuhl, nº 800</p>
							<p><span>bairro:</span>Vila Cláudia - Limeira - SP</p>
							<p><span>telefone:</span>(019) 3442 - 3647</p>
							<p><span>email:</span>contato@dimestra.com.br</p>
							<img class="center-block" src="images/logo-footer.png">
						</div>
					</div>
					<div class="col-md-4 col-sm-4 col-xs-4">
						<div class="endereco">
							<h4>redes sociais:</h4>
							<div class="rsociais-rodape">
								<ul>
									<li><a href="#"><i class="fa fa-facebook-square fa-3x" aria-hidden="true"></i></a></li>
									<li><a href="#"><i class="fa fa-twitter-square fa-3x" aria-hidden="true"></i></a></li>
									<li><a href="#"><i class="fa fa-youtube-square fa-3x" aria-hidden="true"></i></a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="assinatura">
			<div class="container">
				<div class="row">
					<div class="col-md-6 col-sm-6 col-xs-6">
						<div class="copy">
							<p>© Copyright <?php echo date('Y'); ?> | <span>EVENTOS DO DIA</span></p>
						</div>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-6">
						<div class="ass">
							<span>powered by:</span><a href="#">FABIANOMAXIMIANO.COM.BR</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer>
	<script async defer
	src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAeUw9vIn-RKVuTU-1HEbg-gwPfngyk0BE&callback=initMap">
</script>

<div id="fb-root"></div>
<script>
	(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = 'https://connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.11&appId=189731748132666';
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));

	// funçao menu principal

	$(function(){   
		var nav = $('#menu-Principal');  
		$(window).scroll(function () { 
			if ($(this).scrollTop() > 150) { 
				nav.addClass("menu-Fixo");
				document.getElementById("logo").style.display = "none";
				nav.removeClass("menu-Principal")	;
					//document.getElementById("menu-Fixo").style.display = "block";
				} else { 
					//nav.removeClass("menu-Fixo");
					nav.removeClass("menu-Fixo");
					document.getElementById("logo").style.display = "block";
					nav.addClass("menu-Principal");
				} 
			});  
	});


	function header() {
		var url      = window.location.href;     // Retorna a url completa

		if(url != "http://localhost/site/index.php"){
			document.getElementById("diminuir").style.top = "0";
			document.getElementById("diminuir").style.height = "100px";
			document.getElementById("diminuir").style.zIndex = "0";
			document.getElementById("diminuir").style.position = "relative";
		}else{
			document.getElementById("diminuir").style.top = "10px";
			document.getElementById("diminuir").style.height = "100px";
			document.getElementById("diminuir").style.zIndex = "999";
			document.getElementById("diminuir").style.position = "absolute";
		}

		//top: 10%;
    	//position: absolute;
    	//z-index: 999;
    	//width: 100%;	
    }

</script>

</body>
</html> 