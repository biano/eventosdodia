<?php
/**
 * Odin functions and definitions.
 *
 * Sets up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * For more information on hooks, actions, and filters,
 * see http://codex.wordpress.org/Plugin_API
 *
 * @package Odin
 * @since 2.2.0
 */

/**
 * Sets content width.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 600;
}

/**
 * Odin Classes.
 */
require_once get_template_directory() . '/core/classes/class-bootstrap-nav.php';
require_once get_template_directory() . '/core/classes/class-shortcodes.php';
require_once get_template_directory() . '/core/classes/class-thumbnail-resizer.php';
require_once get_template_directory() . '/core/classes/class-post-type.php';
require_once get_template_directory() . '/core/classes/class-taxonomy.php';
require_once get_template_directory() . '/core/classes/class-metabox.php';
//require_once get_template_directory() . '/core/classes/class-shortcodes-menu.php';
// require_once get_template_directory() . '/core/classes/class-theme-options.php';
// require_once get_template_directory() . '/core/classes/class-options-helper.php';
// require_once get_template_directory() . '/core/classes/abstracts/abstract-front-end-form.php';
// require_once get_template_directory() . '/core/classes/class-contact-form.php';
// require_once get_template_directory() . '/core/classes/class-post-form.php';
// require_once get_template_directory() . '/core/classes/class-user-meta.php';
// require_once get_template_directory() . '/core/classes/class-post-status.php';
//require_once get_template_directory() . '/core/classes/class-term-meta.php';

/**
 * Odin Widgets.
 */
require_once get_template_directory() . '/core/classes/widgets/class-widget-like-box.php';

if ( ! function_exists( 'odin_setup_features' ) ) {

	/**
	 * Setup theme features.
	 *
	 * @since 2.2.0
	 */
	function odin_setup_features() {

		/**
		 * Add support for multiple languages.
		 */
		load_theme_textdomain( 'odin', get_template_directory() . '/languages' );

		/**
		 * Register nav menus.
		 */
		register_nav_menus(
			array(
				'main-menu' => __( 'Main Menu', 'odin' )
			)
		);

		/*
		 * Add post_thumbnails suport.
		 */
		add_theme_support( 'post-thumbnails' );

		/**
		 * Add feed link.
		 */
		add_theme_support( 'automatic-feed-links' );

		/**
		 * Support Custom Header.
		 */
		$default = array(
			'width'         => 0,
			'height'        => 0,
			'flex-height'   => false,
			'flex-width'    => false,
			'header-text'   => false,
			'default-image' => '',
			'uploads'       => true,
		);

		add_theme_support( 'custom-header', $default );

		/**
		 * Support Custom Background.
		 */
		$defaults = array(
			'default-color' => '',
			'default-image' => '',
		);

		add_theme_support( 'custom-background', $defaults );

		/**
		 * Support Custom Editor Style.
		 */
		add_editor_style( 'assets/css/editor-style.css' );

		/**
		 * Add support for infinite scroll.
		 */
		add_theme_support(
			'infinite-scroll',
			array(
				'type'           => 'scroll',
				'footer_widgets' => false,
				'container'      => 'content',
				'wrapper'        => false,
				'render'         => false,
				'posts_per_page' => get_option( 'posts_per_page' )
			)
		);

		/**
		 * Add support for Post Formats.
		 */
		// add_theme_support( 'post-formats', array(
		//     'aside',
		//     'gallery',
		//     'link',
		//     'image',
		//     'quote',
		//     'status',
		//     'video',
		//     'audio',
		//     'chat'
		// ) );

		/**
		 * Support The Excerpt on pages.
		 */
		// add_post_type_support( 'page', 'excerpt' );

		/**
		 * Switch default core markup for search form, comment form, and comments to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption'
			)
		);

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for custom logo.
		 *
		 *  @since Odin 2.2.10
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 240,
			'width'       => 240,
			'flex-height' => true,
		) );
	}
}

add_action( 'after_setup_theme', 'odin_setup_features' );

/**
 * Register widget areas.
 *
 * @since 2.2.0
 */
function odin_widgets_init() {
	register_sidebar(
		array(
			'name' => __( 'Main Sidebar', 'odin' ),
			'id' => 'main-sidebar',
			'description' => __( 'Site Main Sidebar', 'odin' ),
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget' => '</aside>',
			'before_title' => '<h3 class="widgettitle widget-title">',
			'after_title' => '</h3>',
		)
	);
}

add_action( 'widgets_init', 'odin_widgets_init' );

/**
 * Flush Rewrite Rules for new CPTs and Taxonomies.
 *
 * @since 2.2.0
 */
function odin_flush_rewrite() {
	flush_rewrite_rules();
}

add_action( 'after_switch_theme', 'odin_flush_rewrite' );

/**
 * Load site scripts.
 *
 * @since 2.2.0
 */
function odin_enqueue_scripts() {
	$template_url = get_template_directory_uri();

	// Loads Odin main stylesheet.
	wp_enqueue_style( 'odin-style', get_stylesheet_uri(), array(), null, 'all' );

	// jQuery.
	wp_enqueue_script( 'jquery' );

	// Html5Shiv
	wp_enqueue_script( 'html5shiv', $template_url . '/assets/js/html5.js' );
	wp_script_add_data( 'html5shiv', 'conditional', 'lt IE 9' );

	// General scripts.
	if ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) {
		// Bootstrap.
		wp_enqueue_script( 'bootstrap', $template_url . '/assets/js/libs/bootstrap.min.js', array(), null, true );

		// FitVids.
		wp_enqueue_script( 'fitvids', $template_url . '/assets/js/libs/jquery.fitvids.js', array(), null, true );

		// Main jQuery.
		wp_enqueue_script( 'odin-main', $template_url . '/assets/js/main.js', array(), null, true );
	} else {
		// Grunt main file with Bootstrap, FitVids and others libs.
		wp_enqueue_script( 'odin-main-min', $template_url . '/assets/js/main.min.js', array(), null, true );
	}

	// Grunt watch livereload in the browser.
	// wp_enqueue_script( 'odin-livereload', 'http://localhost:35729/livereload.js?snipver=1', array(), null, true );

	// Load Thread comments WordPress script.
	if ( is_singular() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}

add_action( 'wp_enqueue_scripts', 'odin_enqueue_scripts', 1 );

/**
 * Odin custom stylesheet URI.
 *
 * @since  2.2.0
 *
 * @param  string $uri Default URI.
 * @param  string $dir Stylesheet directory URI.
 *
 * @return string      New URI.
 */
function odin_stylesheet_uri( $uri, $dir ) {
	return $dir . '/assets/css/style.css';
}

add_filter( 'stylesheet_uri', 'odin_stylesheet_uri', 10, 2 );

/**
 * Query WooCommerce activation
 *
 * @since  2.2.6
 *
 * @return boolean
 */
if ( ! function_exists( 'is_woocommerce_activated' ) ) {
	function is_woocommerce_activated() {
		return class_exists( 'woocommerce' ) ? true : false;
	}
}

/**
 * Core Helpers.
 */
require_once get_template_directory() . '/core/helpers.php';

/**
 * WP Custom Admin.
 */
require_once get_template_directory() . '/inc/admin.php';

/**
 * Comments loop.
 */
require_once get_template_directory() . '/inc/comments-loop.php';

/**
 * WP optimize functions.
 */
require_once get_template_directory() . '/inc/optimize.php';

/**
 * Custom template tags.
 */
require_once get_template_directory() . '/inc/template-tags.php';

/**
 * WooCommerce compatibility files.
 */
if ( is_woocommerce_activated() ) {
	add_theme_support( 'woocommerce' );
	require get_template_directory() . '/inc/woocommerce/hooks.php';
	require get_template_directory() . '/inc/woocommerce/functions.php';
	require get_template_directory() . '/inc/woocommerce/template-tags.php';
}


//taxonomia exemplo
function odin_evento_taxonomy() {
    $evento = new Odin_Taxonomy(
        'Categoria', // Nome (Singular) da nova Taxonomia.
        'categoria', // Slug do Taxonomia.
        'evento' // Nome do tipo de conteúdo que a taxonomia irá fazer parte.
    );

    $evento->set_labels(
        array(
            'menu_name' => __( 'Tipos de evento', 'odin' )
        )
    );

    $evento->set_arguments(
        array(
            'hierarchical' => false
        )
    );
}

add_action( 'init', 'odin_evento_taxonomy', 1 );

//taxonomy

function evento_metabox() {

    $eventos_metabox = new Odin_Metabox(
        'eventos', // Slug/ID of the Metabox (Required)
        'Eventos Mais Detalhes', // Metabox name (Required)
        'evento', // Slug of Post Type (Optional)
        'normal', // Context (options: normal, advanced, or side) (Optional)
        'high' // Priority (options: high, core, default or low) (Optional)
    );

    $eventos_metabox->set_fields(
        array(
            /**
             * Default input examples.
             */

            // Titulo do evento
            array(
                'id'         => 'titulo_evento', // Required
                'label'      => __( 'Titulo Evento', 'odin' ), // Required
                'type'       => 'text', // Required
                'attributes' => array( // Optional (html input elements)
                    'placeholder' => __( 'Informe o titulo do evento!' )
                ),
                // 'default'  => 'Default text', // Optional
                'description' => __( 'Nome do evento para destaque', 'odin' ) // Optional
            ),
            //data do evento
            array(
            	'id'   		  => 'data_evento',
            	'label'       => __('Data do Evento'), 'odin',
            	'type'        => 'text',
            	'attributes'  =>  array(
            		'placeholder' => __( 'Data do evento para destaque', 'odin')
            		),
            ),
            //horario do evento
            array(
            	'id'   		  => 'horario_evento',
            	'label'       => __('Horario do Evento'), 'odin',
            	'type'        => 'text',
            	'attributes'  =>  array(
            		'placeholder' => __( 'Horario do evento para destaque', 'odin')
            		),
            ),
            // Descricao detalhada do evento
            array(
                'id'          => 'descricao_evento', // Required
                'label'       => __( 'Descricao Evento', 'odin' ), // Required
                'type'        => 'textarea', // Required
                'attributes'  => array( // Optional (html input elements)
                    'placeholder' => __( 'Mais detalhes do evento!' )
                ),
                // 'default'  => 'Default text', // Optional
                'description' => __( 'Mais Detalhes do Evento', 'odin' ) // Optional
            ),
            //endereço
            array(
                'id'         => 'endereco_evento', // Required
                'label'      => __( 'Endereço completo', 'odin' ), // Required
                'type'       => 'text', // Required
                'attributes' => array( // Optional (html input elements)
                    'placeholder' => __( 'Informe o endereço onde sera realizado o evento!' )
                ),
                // 'default'  => 'Default text', // Optional
                'description' => __( 'Endereço detalhado do evento', 'odin' ) // Optional
            ),


            //bairro

            array(
                'id'         => 'bairro_evento', // Required
                'label'      => __( 'Bairro', 'odin' ), // Required
                'type'       => 'text', // Required
                'attributes' => array( // Optional (html input elements)
                    'placeholder' => __( 'Informe o bairro onde sera realizado o evento!' )
                ),
                // 'default'  => 'Default text', // Optional
                'description' => __( 'Bairro onde sera realizado evento.', 'odin' ) // Optional
            ),
            
            //referencias
            array(
	            'id'         => 'referencias_evento', // Required
                'label'      => __( 'Indique uma referencia', 'odin' ), // Required
                'type'       => 'text', // Required
                'attributes' => array( // Optional (html input elements)
                    'placeholder' => __( 'Informe uma referencia, para facil localização!' )
                ),
                // 'default'  => 'Default text', // Optional
                'description' => __( 'Indique um ponto de referencia.', 'odin' ) // Optional
            ),


            //cep
            array(
                'id'         => 'cep_evento', // Required
                'label'      => __( 'CEP', 'odin' ), // Required
                'type'       => 'text', // Required
                'attributes' => array( // Optional (html input elements)
                'placeholder' => __( 'Informe o cep somente numeros!' )
                ),
                // 'default'  => 'Default text', // Optional
                //'description' => __( 'Nome do evento para destaque', 'odin' ) // Optional
            ),


            // informaçoes da cidade onde sera realizado evento
            array(
                'id'          => 'cidade_evento', // Required
                'label'       => __( 'Cidade', 'odin' ), // Required
                'type'        => 'text', // Required
                'attributes' => array(
                	'placeholder' => __( 'informe o nome da Cidade onde sera realiazdo evento!')
                ), // Optional (html input elements)
                // 'default'    => 'no', // Optional ('yes' for checked)
                'description' => __( 'Cidade onde sera realizado o evento!', 'odin' ), // Optional
                'add_column' => true,
            ),
            // informar o estado onde sera realizado o evento - EX: SP
            array(
                'id'          => 'estado_evento', // Required
                'label'       => __( 'Estado', 'odin' ), // Required
                'type'        => 'text', // Required
                'attributes' => array(
                	'placeholder' => __( 'informe o Estado onde sera realiazdo evento!')
                ), // Optional (html input elements)
                // 'default'    => 'no', // Optional ('yes' for checked)
                'description' => __( 'Estado onde sera realizado o evento!', 'odin' ), // Optional

                'add_column' => true,
            ),
            //Informar o valor do evento
            array(
                'id'          => 'valor_evento', // Required
                'label'       => __( 'Valor', 'odin' ), // Required
                'type'        => 'text', // Required
                'attributes' => array(
                	'placeholder' => __( 'informe o Estado onde sera realiazdo evento!')
                ), // Optional (html input elements)
                // 'default'    => 'no', // Optional ('yes' for checked)
                'description' => __( 'Estado onde sera realizado o evento!', 'odin' ), // Optional

                'add_column' => true,
            )

        )
    );
}

add_action( 'init', 'evento_metabox', 1 );

function odin_evento_cpt() {
    $evento = new Odin_Post_Type(
        'evento', // Nome (Singular) do Post Type.
        'evento' // Slug do Post Type.
    );

    $evento->set_labels(
        array(
            'menu_name' => __( 'Meus eventos', 'odin' ),
            'all_items' => __( 'Todos eventos', 'odin' ),
            'add_new'   => __( 'Novo evento', 'odin' ),
            'edit_post' => __('Editar Evento')
        )
    );

    $evento->set_arguments(
        array(
            'supports' => array( 'title', 'thumbnail' ),
            'menu_icon' => 'dashicons-tickets'
        )
    );
}

add_action( 'init', 'odin_evento_cpt', 1 );

//imagens 
$maior = odin_thumbnail( 400, 300, 'Meu texto alternativo', true, 'minha-classe' );

$testeimagem = odin_get_image_url( $id, 800, 300, true, true );

//imagens thumbnail
add_theme_support( 'post-thumbnails' );

//imagens com tamanhos personalizados
add_image_size( $name, $width, $height, $crop );

// Vai cortar a imagem para exatamente 600 px de largura por 400 px de altura.
add_image_size( 'imagem-slide', 1024, 400, true );
 
// Vai gerar uma imagem com 300 px de largura e altura proporcional sem distorcer nada.
add_image_size( 'thumb-index', 400, 250);
add_image_size( 'miniaturas', 164, 150);
add_image_size( 'imagem-media', 900, 350);
