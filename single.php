<?php 
/*
Template Name: Contato
*/
	//Carrega o cabeçalho do site
	get_header();
?>

	<section class="conteudo">
		<div class="container">
			<h3>Detalhes do evento</h3>
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<figure>
						<?php the_post_thumbnail( 'imagem-media' ); ?>
						<figcaption>
							<h2>
								<span><?php echo get_post_meta( get_the_ID(), 'estado_evento', true ); ?></span>
								<span><?php echo get_post_meta( get_the_ID(), 'titulo_evento', true ); ?></span></h2></figcaption>
					</figure>
				</div>
				<div class="col-md-8 col-sm-8 col-xs-8"">
					<h3><?php the_title(); ?></h3>
					<p><?php echo get_post_meta( get_the_ID(), 'descricao_evento', true ); ?></p>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-4 teste">
					<h3>Redes Sociais</h3>
					<ul>
						<li>
							<button type="button" class="btn btn-facebook"><i class="fa fa-facebook fa-3x" aria-hidden="true"></i>facebook</button>
						</li>
						<li>
							<button type="button" class="btn btn-twitter"><i class="fa fa-twitter fa-3x" aria-hidden="true"></i>twiitter</button>
						</li>
						<li>
							<button type="button" class="btn btn-google"><i class="fa fa-google fa-3x" aria-hidden="true"></i>google</button>
						</li>

					</ul>
					
				</div>
			</div>
			<h3>Mais Detalhes</h3>
			<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-6 classBorder">
				<h3>Mais informações</h3>
				<h6>Data do Evento</h6>
				<p><?php echo get_post_meta( get_the_ID(),'data_evento', true ); ?></p>
				<h6>Horario do Evento</h6>
				<p><?php echo get_post_meta( get_the_ID(),'horario_evento', true ); ?></p>
				<h6>Endereço do Evento</h6>
				<p><?php echo get_post_meta( get_the_ID(),'Endereco_evento', true ); ?></p>
				<h6>Bairro<h6>
				<p><?php echo get_post_meta( get_the_ID(),'bairro_evento', true ); ?></p>
				<h6>CEP</h6>
				<p><?php echo get_post_meta( get_the_ID(),'cep_evento', true ); ?></p>
				<h6>Cidade</h6>
				<p><?php echo get_post_meta( get_the_ID(),'cidade_evento', true ); ?></p>
				<h6>Valor</h6>
				<p><?php echo get_post_meta( get_the_ID(),'valor_evento', true ); ?></p>
			</div>
			<div class="col-md-5 col-sm-5 col-xs-5 classBorder">
				<h3>Referencia</h3>
			</div>
			</div>
		</div>
		
	</section>

<?php
	//Carrega o rodape do site 
	get_footer();
?>