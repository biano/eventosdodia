<footer>
	<div class="rodape">
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-sm-4 col-xs-4">
					<div class="endereco">
						<h4>acesso facil:</h4>
						<ul>
							<li><a href="#">home</a></li>
							<li><a href="#">sobre empresa</a></li>
							<li><a href="#">cadastrar evento</a></li>
							<li><a href="#">principais eventos</a></li>
							<li><a href="#">contato</a></li>
						</ul>
					</div>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-4">
					<div class="endereco">
						<h4>nossa localização:</h4>
						<p><span>endereço:</span>Rua Benedito Kuhl, nº 800</p>
						<p><span>bairro:</span>Vila Cláudia - Limeira - SP</p>
						<p><span>telefone:</span>(019) 3442 - 3647</p>
						<p><span>email:</span>contato@dimestra.com.br</p>
						<a href="<?php echo get_settings('home'); ?>"><img class="center-block" src="<?php bloginfo('template_directory'); ?>/assets/images/logo-footer.png"></a>
					</div>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-4">
					<div class="endereco">
						<h4>redes sociais:</h4>
						<div class="rsociais-rodape">
							<ul>
								<li><a href="#"><i class="fa fa-facebook-square fa-3x" aria-hidden="true"></i></a></li>
								<li><a href="#"><i class="fa fa-twitter-square fa-3x" aria-hidden="true"></i></a></li>
								<li><a href="#"><i class="fa fa-youtube-square fa-3x" aria-hidden="true"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="assinatura">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-sm-6 col-xs-6">
					<div class="copy">
						<p>© Copyright <?php echo date('Y'); ?> | <span>EVENTOS DO DIA</span></p>
					</div>
				</div>
				<div class="col-md-6 col-sm-6 col-xs-6">
					<div class="ass">
						<span>powered by:</span><a href="#">FABIANOMAXIMIANO.COM.BR</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>
<!--
<script async defer
src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAeUw9vIn-RKVuTU-1HEbg-gwPfngyk0BE&callback=initMap">
</script>
-->
<div id="fb-root"></div>
<script>
	(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = 'https://connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.11&appId=189731748132666';
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));

	// funçao menu principal

	$(function(){   
		var nav = $('#menu-Principal');  
		$(window).scroll(function () { 
			if ($(this).scrollTop() > 150) { 
				nav.addClass("menu-Fixo");
				document.getElementById("logo").style.display = "none";
				nav.removeClass("menu-Principal")	;
					//document.getElementById("menu-Fixo").style.display = "block";
				} else { 
					//nav.removeClass("menu-Fixo");
					nav.removeClass("menu-Fixo");
					document.getElementById("logo").style.display = "block";
					nav.addClass("menu-Principal");
				} 
			});  
	});


	function header() {
		var url      = window.location.href;     // Retorna a url completa

		if(url != "http://localhost/site/index.php"){
			document.getElementById("diminuir").style.top = "0";
			document.getElementById("diminuir").style.height = "100px";
			document.getElementById("diminuir").style.zIndex = "0";
			document.getElementById("diminuir").style.position = "relative";
		}else{
			document.getElementById("diminuir").style.top = "-10px";
			document.getElementById("diminuir").style.height = "100px";
			document.getElementById("diminuir").style.zIndex = "999";
			document.getElementById("diminuir").style.position = "absolute";
		}

		//top: 10%;
    	//position: absolute;
    	//z-index: 999;
    	//width: 100%;	
	}

</script>

</body>
</html> 