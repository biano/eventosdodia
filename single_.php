<?php
/**
 * The Template for displaying all single posts.
 *
 * @package Odin
 * @since 2.2.0
 */

get_header(); ?>
<section>
	<?php echo odin_thumbnail( 800, 200, 'Meu texto alternativo', true, 'minha-classe' ); ?>
</section>
<section>
	<div class="container">
		<h1><?php the_title(); ?></h1>
	        	<?php echo "<br>"; ?>
				<h2><?php echo get_post_meta( get_the_ID(), 'titulo_evento', true ) ."</h2><br>"; ?>
				<?php echo get_post_meta( get_the_ID(), 'descricao_evento', true )."<br>"; ?>
				<?php echo get_post_meta( get_the_ID(), 'endereco_evento', true ) ."<br>"; ?>
				<?php echo get_post_meta( get_the_ID(), 'bairro_evento', true ) ."<br>"; ?>
				<?php echo get_post_meta( get_the_ID(), 'referencias_evento', true ) ."<br>";?>			
				<?php echo get_post_meta( get_the_ID(), 'cep_evento', true ) ."<br>"; ?>
				<?php echo get_post_meta( get_the_ID(), 'cidade_evento', true ) ."<br>"; ?>
				<?php echo get_post_meta( get_the_ID(), 'estado_evento', true ) ."<br>"; ?>
				<?php echo get_post_meta( get_the_ID(), 'valor_evento', true ) ."<br>"; ?>
			<?php?>

	</div>
</section>

<?php
get_footer();
